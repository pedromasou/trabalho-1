#ifndef ALUNO_H
#define ALUNO_H
#include "pessoa.h"

class Aluno:public Pessoa
{
    Q_OBJECT
public:
    Aluno();
    Aluno (QString p_cpf,QString p_nome, QString p_MatriculaALUNO, QString p_codCurso);
    char matriculaALUNO [5];
};

#endif // ALUNO_H
