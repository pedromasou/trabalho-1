#ifndef FACULDADE_H
#define FACULDADE_H

#include <QObject>
#include <QList>
#include "curso.h"
#include "disciplina.h"
#include "aluno.h"
#include "professor.h"
#include "turma.h"


class Faculdade : public QObject
{
    Q_OBJECT
public:

    //explicit Faculdade(QObject *parent = nullptr);

    Faculdade();

    QList<Aluno*> v_alunos;
    QList<Professor*> v_professores;
    QList<Curso*> v_cursos;
    QList<Disciplina*> v_disciplinas;
    QList<Turma*> v_turmas;

    bool cadastrarCurso(QString p_codigoCurso, QString p_nomeCurso);

    bool cadastrarDisciplina(QString p_codigoDisciplina,
                             QString p_nomeDisciplina, QString p_codigoCurso);

    bool cadastrarAluno(QString p_matriculaAluno, QString p_nomeAluno,
                        QString p_cpfAluno, QString p_codigoCurso);

    bool cadastrarProfessor(QString p_matriculaProfessor, QString p_nomeProfessor,
                            QString p_cpfProfessor,QString p_codigoCurso);

    bool cadastrarTurmaADisciplina(QString p_codigoTurma, QString p_codigoDisciplina, QString p_codigoProfessor);


    bool addAlunoATurma(QString p_codigoAluno, QString p_codigoTurma);
    bool addProfessorATurma(QString p_codigoProfessor, QString p_codigoTurma);


private:

    Curso* existeCurso(QString p_codigoCurso);
    Disciplina* existeDisciplina(QString p_codigoDisciplina);
    Aluno* existeAluno(QString p_codigoAluno);
    Professor* existeProfessor(QString p_codigoProfessor);
    Turma* existeTurma(QString p_codigoTurma);

signals:

public slots:
};

#endif // FACULDADE_H
