#ifndef PROFESSOR_H
#define PROFESSOR_H
#include <QVector>
#include "pessoa.h"
#include <QObject>

class Professor: public Pessoa
{
    Q_OBJECT
public:
    Professor();
    Professor(QString p_cpf, QString p_nome ,QString p_MatriculaProf, QString p_codCurso);
    char MatriculaProf [5];

};

#endif // PROFESSOR_H
