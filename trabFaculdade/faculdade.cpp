#include "faculdade.h"

Faculdade::Faculdade()
{

}

bool Faculdade::cadastrarCurso(QString p_codigoCurso, QString p_nomeCurso)
{

    if(existeCurso(p_codigoCurso) == nullptr)
    {
        Curso* cursoAInserir = new Curso(p_codigoCurso, p_nomeCurso);
        v_cursos.append(cursoAInserir);

        return true;
    }

    return false;
}

bool Faculdade::cadastrarDisciplina(QString p_codigoDisciplina, QString p_nomeDisciplina, QString p_codigoCurso)
{

    if(existeDisciplina(p_codigoDisciplina) == nullptr)
    {
        Disciplina* disciplinaAInserir = new Disciplina(p_codigoDisciplina, p_nomeDisciplina, p_codigoCurso);

        Curso* cursoAInserir = existeCurso(p_codigoCurso);

        if( cursoAInserir != nullptr )
        {
                v_disciplinas.append(disciplinaAInserir);
                cursoAInserir->addDisciplina(v_disciplinas.last());
                return true;

        }

    }
    return false;
}

bool Faculdade::cadastrarAluno(QString p_matriculaAluno, QString p_nomeAluno,
                               QString p_cpfAluno,QString p_codigoCurso)
{

    if(existeAluno(p_matriculaAluno) == nullptr)
    {
        Aluno* alunoAInserir = new Aluno(p_cpfAluno, p_nomeAluno,
                                         p_matriculaAluno, p_codigoCurso);

        Curso* cursoAInserir = existeCurso(p_codigoCurso);

        if( cursoAInserir != nullptr)
        {
            v_alunos.append(alunoAInserir);
            cursoAInserir->addAluno(v_alunos.last());
            return true;
        }

    }
    return false;
}


bool Faculdade::cadastrarProfessor(QString p_matriculaProfessor, QString p_nomeProfessor,
                                   QString p_cpfProfessor, QString p_codigoCurso)
{
    if(existeProfessor(p_matriculaProfessor) == nullptr)
    {
        Professor* professorAInserir = new Professor(p_cpfProfessor, p_nomeProfessor,
                                                     p_matriculaProfessor, p_codigoCurso);

        Curso* cursoAInserir = existeCurso(p_codigoCurso);

        if( cursoAInserir != nullptr)
        {
                v_professores.append(professorAInserir);
                cursoAInserir->addProf(v_professores.last());
                return true;
        }
    }
    return false;
}

bool Faculdade::cadastrarTurmaADisciplina(QString p_codigoTurma, QString p_codigoDisciplina,
                                          QString p_codigoProfessor)
{

    if(existeTurma(p_codigoTurma) == nullptr)
    {
        Turma *turmaAInserir = new Turma(p_codigoTurma, existeProfessor(p_codigoProfessor), p_codigoDisciplina);

        Disciplina* disciplinaAInserir = existeDisciplina(p_codigoDisciplina);

        if( disciplinaAInserir != nullptr)
        {
                v_turmas.append(turmaAInserir);
                disciplinaAInserir->addTurma(v_turmas.last());
                return true;
        }
    }
    return false;
}

Curso* Faculdade::existeCurso(QString p_codigoCurso)
{

    for (int a = 0; a < v_cursos.length(); a++)
    {
        if (QString(v_cursos.at(a)->codigoCurso) == p_codigoCurso)
        {
            return v_cursos[a];
        }
    }

    return nullptr;
}

Disciplina* Faculdade::existeDisciplina(QString p_codigoDisciplina)
{

    for(int a = 0; a < v_disciplinas.length(); a++)
    {
        if(QString(v_disciplinas.at(a)->CodigoDisc) == p_codigoDisciplina)
        {
            return v_disciplinas[a];
        }
    }

    return nullptr;
}

Aluno* Faculdade::existeAluno(QString p_codigoAluno)
{

    for(int a = 0; a < v_alunos.length(); a++)
    {
        if(QString(v_alunos.at(a)->matriculaALUNO) == p_codigoAluno)
        {
            return v_alunos[a];
        }
    }

    return nullptr;
}

Professor* Faculdade::existeProfessor(QString p_codigoProfessor)
{

    for(int a = 0; a < v_professores.length(); a++)
    {
        if(QString(v_professores.at(a)->MatriculaProf) == p_codigoProfessor)
        {
            return v_professores[a];
        }
    }

    return nullptr;
}

Turma* Faculdade::existeTurma(QString p_codigoTurma)
{
    for(int a = 0; a < v_turmas.length(); a++)
    {
        if(QString(v_turmas.at(a)->codigoTurma) == p_codigoTurma)
        {
            return v_turmas[a];
        }
    }
    return nullptr;
}
