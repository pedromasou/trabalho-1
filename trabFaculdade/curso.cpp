#include "curso.h"

Curso::Curso(QString p_codigoCurso, QString p_nomeCurso)
{
    const QByteArray cod_Curso = p_codigoCurso.toUtf8();
    codigoCurso[qMin(8,cod_Curso.size())]='\0';
    std::copy(cod_Curso.constBegin(),
              cod_Curso.constBegin()+qMin(8,cod_Curso.size()),
              codigoCurso);

    const QByteArray nome_Curso = p_nomeCurso.toUtf8();
    nomeCurso[qMin(50,nome_Curso.size())]='\0';
    std::copy(nome_Curso.constBegin(),
              nome_Curso.constBegin()+qMin(50,nome_Curso.size()),
              nomeCurso);

}


void Curso::addAluno(Aluno *Alu)
{
    ListaAluno.append(Alu);
}


void Curso::addDisciplina(Disciplina *Disc)
{
    Lista_Disciplina.append(Disc);
}


void Curso::addProf(Professor *Prof)
{

    Lista_Professor.append(Prof);
}


Aluno* Curso::alunoJaINserido(QString p_codigoAluno)
{
    for(int i=0;i<ListaAluno.size();i++){
        if(ListaAluno.at(i)->matriculaALUNO==p_codigoAluno)
            return ListaAluno.at(i);
        }
    return nullptr;
}


Disciplina* Curso::disciplinaJaInserida(QString p_codigoDisc)
{
    for(int i=0;i<Lista_Disciplina.size();i++){
        if(Lista_Disciplina.at(i)->CodigoDisc==p_codigoDisc)
            return Lista_Disciplina.at(i);
        }
    return nullptr;
}


Professor* Curso::professorJaInserido(QString p_matriculaProf)
{
    for(int i=0;i<Lista_Professor.size();i++){
        if(Lista_Professor.at(i)->MatriculaProf==p_matriculaProf)
            return Lista_Professor.at(i);
        }
    return nullptr;
}

