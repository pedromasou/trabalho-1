#include "professor.h"
#include <cstring>
#include <string>

Professor::Professor()
{
    *Nome='\0';
    *CPF='\0';
    *MatriculaProf='\0';

}
Professor::Professor (QString p_cpf,QString p_nome, QString p_MatriculaProf, QString p_codCurso)
{
    const QByteArray Nome_PROF = p_nome.toUtf8();
    Nome[qMin(30,Nome_PROF.size())]='\0';
    std::copy(Nome_PROF.constBegin(),
              Nome_PROF.constBegin()+qMin(30,Nome_PROF.size()),
              Nome);

    const QByteArray cpf_PROF = p_cpf.toUtf8();
    CPF[qMin(12,cpf_PROF.size())]='\0';
    std::copy(cpf_PROF.constBegin(),
              cpf_PROF.constBegin()+qMin(12,cpf_PROF.size()),
              CPF);

    const QByteArray Matricula_PROF = p_MatriculaProf.toUtf8();
    MatriculaProf[qMin(5,Matricula_PROF.size())]='\0';
    std::copy(Matricula_PROF.constBegin(),
              Matricula_PROF.constBegin()+qMin(5,Matricula_PROF.size()),
              MatriculaProf);

    const QByteArray cd_Curso = p_codCurso.toUtf8();
    codCurso[qMin(8,cd_Curso.size())]='\0';
    std::copy(cd_Curso.constBegin(),
              cd_Curso.constBegin()+qMin(8,cd_Curso.size()),
              codCurso);

}
