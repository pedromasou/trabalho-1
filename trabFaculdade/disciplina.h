#ifndef DISCIPLINA_H
#define DISCIPLINA_H

#include <QList>
#include "turma.h"

class Disciplina:public QObject
{
    Q_OBJECT
public:
    Disciplina();
    Disciplina(QString p_CodigoTurmas, QString p_NomeDisciplina, QString p_codCurso);
    void addTurma(Turma *Turmas_Disci);
    Turma* TurmaJaInserida(QString p_CodigoTurmas);
    char CodigoDisc[10];
    char nomeDisc[60];
    char codCurso[8];

//private:

    QList<Turma*> Lista_Turmas;

};

#endif // DISCIPLINA_H
