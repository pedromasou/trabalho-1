#include "intfaculdade.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    IntFaculdade w;
    w.show();

    return a.exec();
}
