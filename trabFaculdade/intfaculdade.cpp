#include "intfaculdade.h"
#include "ui_intfaculdade.h"
#include <QMessageBox>

IntFaculdade::IntFaculdade(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::IntFaculdade)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    ui->tabWidget->setCurrentIndex(0);
}

IntFaculdade::~IntFaculdade()
{
    delete ui;
}

void IntFaculdade::atualizarCB()
{
    ui->cbProfessor->clear();
    ui->cbCursoAluno->clear();
    ui->cbCursoProfessor->clear();
    ui->cbCurso->clear();
    ui->cbCursoDisciplina->clear();
    ui->cbDisciplina->clear();
    ui->cbTurma->clear();
    ui->cbAluno->clear();

    //EXEMPLO PREENCHENDO A COMBO BOX
    QString v_professoresNomes[4] = {"Lucio", "Rodrigo", "Janaina"};
    QString v_professoresCods[4] = {"1", "45", "654"};

    for(int i = 0; i < 3; i++)
    {
        ui->cbProfessor->addItem(QIcon(":/rec/icones/666201.png"),
                                 v_professoresCods[i] + " " + v_professoresNomes[i]);
    }



    QString v_cursosNomes[4] = {"Eng Comp", "Eng Elet", "Medicina"};
    QString v_cursosCods[4] = {"10", "15", "20"};
    for(int i = 0; i < 3; i++)
    {
        ui->cbCursoAluno->addItem(v_cursosCods[i] + " " + v_cursosNomes[i]);
        ui->cbCursoProfessor->addItem(v_cursosCods[i] + " " + v_cursosNomes[i]);
        ui->cbCursoDisciplina->addItem(v_cursosCods[i] + " " + v_cursosNomes[i]);
        ui->cbCurso->addItem(v_cursosCods[i] + " " + v_cursosNomes[i]);
    }

}

void IntFaculdade::on_addProfessor_clicked()
{
    QMessageBox::information(this,"title",ui->cbProfessor->currentText() + " Adicionado");
    //QMessageBox::information(this,"title",QString::number(ui->cbProfessor->currentIndex()));
}

void IntFaculdade::on_okCurso_clicked()
{
    //ao dar ok no curso, preenche combo box da disciplina com as relacionada ao curso e habilita
    if(!(ui->cbCurso->currentText().trimmed().isNull()
            || ui->cbCurso->currentText().trimmed().isEmpty()))
    {
        ui->label_12->setEnabled(true);
        ui->cbDisciplina->setEnabled(true);
        ui->okDisciplina->setEnabled(true);
    }
}

void IntFaculdade::on_okDisciplina_clicked()
{
    if(!(ui->cbDisciplina->currentText().trimmed().isNull()
            || ui->cbDisciplina->currentText().trimmed().isEmpty()))
    {
    //ao dar ok na disciplina, preenche combo box da turma com as relacionada a disciplina e habilita
        ui->label_22->setEnabled(true);
        ui->cbTurma->setEnabled(true);
        ui->okTurma->setEnabled(true);
    }

}

void IntFaculdade::on_okTurma_clicked()
{
    if(!(ui->cbTurma->currentText().trimmed().isNull()
            || ui->cbTurma->currentText().trimmed().isEmpty()))
    {
    ui->cbAluno->setEnabled(true);
    ui->addAluno->setEnabled(true);
    }

}

void IntFaculdade::on_addAluno_clicked()
{
    //cada adição de um aluno atualiza o textEdit
}

void IntFaculdade::on_actionCadastro_triggered()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->actionMatricula->setChecked(false);
    ui->actionBancoDados->setChecked(false);
}

void IntFaculdade::on_actionMatricula_triggered()
{
    ui->stackedWidget->setCurrentIndex(1);
    ui->actionCadastro->setChecked(false);
    ui->actionBancoDados->setChecked(false);
}

void IntFaculdade::on_actionBancoDados_triggered()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->actionCadastro->setChecked(false);
    ui->actionMatricula->setChecked(false);
}

void IntFaculdade::on_cadastrarCurso_clicked()
{
    if(!(ui->codCurso->text().trimmed().isNull() || ui->codCurso->text().trimmed().isEmpty())
            || (ui->nomeCurso->text().trimmed().isNull() || ui->nomeCurso->text().trimmed().isEmpty()))
    {
        oFaculdade.cadastrarCurso(ui->codCurso->text().trimmed(), ui->nomeCurso->text().trimmed());
        cleanCurso();
        resetAllCursosCb();
    }

}

void IntFaculdade::on_cadastrarAluno_clicked()
{
    if (!(ui->codAluno->text().trimmed().isNull() || ui->codAluno->text().trimmed().isEmpty()
            || ui->nomeAluno->text().trimmed().isNull() || ui->nomeAluno->text().trimmed().isEmpty()
            || ui->cpfAluno->text().trimmed().isNull() || ui->cpfAluno->text().trimmed().isEmpty()
            || ui->cbCursoAluno->currentText().split("-").at(0).trimmed().isNull()
            || ui->cbCursoAluno->currentText().split("-").at(0).trimmed().isEmpty()))
    {
        oFaculdade.cadastrarAluno(ui->codAluno->text().trimmed(),
                                  ui->nomeAluno->text().trimmed(),
                                  ui->cpfAluno->text().trimmed(),
                                  ui->cbCursoAluno->currentText().split("-").at(0).trimmed());
        cleanAluno();
        resetAllAlunosCb();
    }

}

void IntFaculdade::on_cadastrarProfessor_clicked()
{
    if (!(ui->codProfessor->text().trimmed().isNull() || ui->codProfessor->text().trimmed().isEmpty()
            || ui->nomeProfessor->text().trimmed().isNull() || ui->cpfProfessor->text().trimmed().isEmpty()
            || ui->cpfProfessor->text().trimmed().isNull() || ui->cpfProfessor->text().trimmed().isEmpty()
            || ui->cbCursoProfessor->currentText().split("-").at(0).trimmed().isNull()
            || ui->cbCursoProfessor->currentText().split("-").at(0).trimmed().isEmpty()))
    {
        oFaculdade.cadastrarProfessor(ui->codProfessor->text().trimmed(),
                                  ui->nomeProfessor->text().trimmed(),
                                  ui->cpfProfessor->text().trimmed(),
                                  ui->cbCursoProfessor->currentText().split("-").at(0).trimmed());

        cleanProfessor();
        resetAllProfessoresCb();
    }

}

void IntFaculdade::on_cadastrarDisciplina_clicked()
{
    if (!(ui->codDisciplina->text().trimmed().isNull() || ui->codDisciplina->text().trimmed().isEmpty()
            || ui->nomeDisciplina->text().trimmed().isNull() || ui->nomeDisciplina->text().trimmed().isEmpty()
            || ui->cbCursoDisciplina->currentText().split("-").at(0).trimmed().isNull()
            || ui->cbCursoDisciplina->currentText().split("-").at(0).trimmed().isEmpty()))
    {
        oFaculdade.cadastrarDisciplina(ui->codDisciplina->text().trimmed(),
                                       ui->nomeDisciplina->text().trimmed(),
                                       ui->cbCursoDisciplina->currentText().split("-").at(0).trimmed());
        cleanDisciplina();
        resetAllDisciplinasCb();
    }
}

void IntFaculdade::on_cadastrarTurma_clicked()
{
    if (!(ui->codTurma->text().trimmed().isNull() || ui->codTurma->text().trimmed().isEmpty()
           || ui->cbDisciplinaTurma->currentText().split("-").at(0).trimmed().isNull()
           || ui->cbDisciplinaTurma->currentText().split("-").at(0).trimmed().isEmpty()
           || ui->cbProfessor->currentText().split("-").at(0).trimmed().isNull()
           || ui->cbProfessor->currentText().split("-").at(0).trimmed().isEmpty()))
    {
        oFaculdade.cadastrarTurmaADisciplina(ui->codTurma->text().trimmed(),
                                             ui->cbDisciplinaTurma->currentText().split("-").at(0).trimmed(),
                                             ui->cbProfessor->currentText().split("-").at(0).trimmed());
        cleanTurma();
        resetAllTurmasCb();
    }
}

/*///////////////////////////////////////////
FUNCOES SLOT PARA LIMPEZA DE CAMPOS DE CADASTRO
///////////////////////////////////////////*/

void IntFaculdade::on_clearCurso_clicked()
{
    cleanCurso();
}

void IntFaculdade::on_clearAluno_clicked()
{
    cleanAluno();
}

void IntFaculdade::on_clearProfessor_clicked()
{
    cleanProfessor();
}

void IntFaculdade::on_clearDisciplina_clicked()
{
    cleanDisciplina();
}

void IntFaculdade::on_clearTurma_clicked()
{
    cleanTurma();
}


/*///////////////////////////////////////////
FUNCOES PRIVATE PARA LIMPEZA DE CADASTROS
///////////////////////////////////////////*/
void IntFaculdade::cleanCurso()
{
    ui->codCurso->clear();
    ui->nomeCurso->clear();
}

void IntFaculdade::cleanAluno()
{
    ui->codAluno->clear();
    ui->nomeAluno->clear();
    ui->cpfAluno->clear();
    ui->cbCursoAluno->setCurrentIndex(0);
}

void IntFaculdade::cleanProfessor()
{
    ui->codProfessor->clear();
    ui->nomeProfessor->clear();
    ui->cpfProfessor->clear();
    ui->cbCursoProfessor->setCurrentIndex(0);
}

void IntFaculdade::cleanDisciplina()
{
    ui->codDisciplina->clear();
    ui->nomeDisciplina->clear();
    ui->cbCursoDisciplina->setCurrentIndex(0);
}

void IntFaculdade::cleanTurma()
{
    ui->codTurma->clear();
    ui->cbDisciplinaTurma->setCurrentIndex(0);
    ui->cbProfessor->setCurrentIndex(0);
}

/*///////////////////////////////////////////
FUNCOES PRIVATE PARA LIMPEZA DE COMBOBOX GERAIS
///////////////////////////////////////////*/
void IntFaculdade::resetAllCursosCb()
{
    ui->cbCurso->clear();
    ui->cbCursoAluno->clear();
    ui->cbCursoProfessor->clear();
    ui->cbCursoDisciplina->clear();

    for (int a = 0; a < oFaculdade.v_cursos.length(); a++)
    {
        ui->cbCurso->addItem(QString(oFaculdade.v_cursos.at(a)->codigoCurso)
                                       + " - "
                                       + QString(oFaculdade.v_cursos.at(a)->nomeCurso));

        ui->cbCursoAluno->addItem(QString(oFaculdade.v_cursos.at(a)->codigoCurso)
                                       + " - "
                                       + QString(oFaculdade.v_cursos.at(a)->nomeCurso));

        ui->cbCursoProfessor->addItem(QString(oFaculdade.v_cursos.at(a)->codigoCurso)
                                       + " - "
                                       + QString(oFaculdade.v_cursos.at(a)->nomeCurso));

        ui->cbCursoDisciplina->addItem(QString(oFaculdade.v_cursos.at(a)->codigoCurso)
                                       + " - "
                                       + QString(oFaculdade.v_cursos.at(a)->nomeCurso));
    }

}

void IntFaculdade::resetAllAlunosCb()
{
    QString *tempCurso = new QString(ui->cbCurso->currentText().split("-").at(0).trimmed());
    resetMatriculaAlunosCb(tempCurso);
    delete tempCurso;
}

void IntFaculdade::resetAllProfessoresCb()
{
    ui->cbProfessor->clear();
    for(int a = 0; a < oFaculdade.v_professores.length(); a++)
    {

        ui->cbProfessor->addItem(QString(oFaculdade.v_professores.at(a)->MatriculaProf)
                                 + QString(" - ")
                                 + QString(oFaculdade.v_professores.at(a)->Nome));

    }
}

void IntFaculdade::resetAllDisciplinasCb()
{
    ui->cbDisciplinaTurma->clear();
    for(int a = 0; a < oFaculdade.v_disciplinas.length(); a++)
    {
        ui->cbDisciplinaTurma->addItem(QString(oFaculdade.v_disciplinas.at(a)->CodigoDisc)
                                 + QString(" - ")
                                 + QString(oFaculdade.v_disciplinas.at(a)->nomeDisc));

    }

    QString *tempCurso = new QString(ui->cbCurso->currentText().split("-").at(0).trimmed());
    resetMatriculaDisciplinasCb(tempCurso);
    delete tempCurso;
}

void IntFaculdade::resetAllTurmasCb()
{
    QString *tempDisc = new QString(ui->cbDisciplina->currentText().split("-").at(0).trimmed());
    resetMatriculaTurmasCb(tempDisc);
    delete tempDisc;
}

/*///////////////////////////////////////////
FUNCOES PRIVATE PARA LIMPEZA DE COMBOBOX DA TELA DE MATRICULAS
///////////////////////////////////////////*/
void IntFaculdade::resetMatriculaAlunosCb(QString *cdCurso)
{
    ui->cbAluno->clear();
    for(int a = 0; a < oFaculdade.v_alunos.length(); a++)
    {
        if(*cdCurso ==oFaculdade.v_alunos.at(a)->codCurso)
        {
            ui->cbAluno->addItem(QString(oFaculdade.v_alunos.at(a)->matriculaALUNO)
                                 + QString(" - ")
                                 + QString(oFaculdade.v_alunos.at(a)->Nome));
        }
    }
}

void IntFaculdade::resetMatriculaDisciplinasCb(QString *cdCurso)
{
    ui->cbDisciplina->clear();
    for(int a = 0; a < oFaculdade.v_disciplinas.length(); a++)
    {
        if(*cdCurso ==oFaculdade.v_disciplinas.at(a)->codCurso)
        {
        ui->cbDisciplina->addItem(QString(oFaculdade.v_disciplinas.at(a)->CodigoDisc)
                                 + QString(" - ")
                                 + QString(oFaculdade.v_disciplinas.at(a)->nomeDisc));
        }
    }
}

void IntFaculdade::resetMatriculaTurmasCb(QString *cdDisciplina)
{
    ui->cbTurma->clear();
    for(int a = 0; a < oFaculdade.v_turmas.length(); a++)
    {
        if(*cdDisciplina ==oFaculdade.v_turmas.at(a)->codigoDisc)
        {
        ui->cbTurma->addItem(QString(oFaculdade.v_turmas.at(a)->codigoTurma));
        }
    }
}

void IntFaculdade::on_cbCurso_currentIndexChanged(int index)
{
    QString *tempCurso = new QString(ui->cbCurso->currentText().split("-").at(0).trimmed());
    resetMatriculaDisciplinasCb(tempCurso);

    QString *tempDisc = new QString(ui->cbDisciplina->currentText().split("-").at(0).trimmed());
    resetMatriculaTurmasCb(tempDisc);

    resetMatriculaAlunosCb(tempCurso);
    delete tempCurso;
    delete tempDisc;

    ui->label_12->setEnabled(false);
    ui->cbDisciplina->setEnabled(false);
    ui->okDisciplina->setEnabled(false);

    ui->label_22->setEnabled(false);
    ui->cbTurma->setEnabled(false);
    ui->okTurma->setEnabled(false);

    ui->cbAluno->setEnabled(false);
    ui->addAluno->setEnabled(false);
}

void IntFaculdade::on_cbDisciplina_currentIndexChanged(int index)
{
    QString *tempDisc = new QString(ui->cbDisciplina->currentText().split("-").at(0).trimmed());
    resetMatriculaTurmasCb(tempDisc);
    delete tempDisc;

    ui->label_22->setEnabled(false);
    ui->cbTurma->setEnabled(false);
    ui->okTurma->setEnabled(false);

    ui->cbAluno->setEnabled(false);
    ui->addAluno->setEnabled(false);
}
