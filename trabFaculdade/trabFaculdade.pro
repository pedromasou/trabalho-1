#-------------------------------------------------
#
# Project created by QtCreator 2019-09-28T01:36:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = trabFaculdade
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        aluno.cpp \
        curso.cpp \
        disciplina.cpp \
        faculdade.cpp \
        main.cpp \
        intfaculdade.cpp \
        pessoa.cpp \
        professor.cpp \
        turma.cpp

HEADERS += \
        aluno.h \
        curso.h \
        disciplina.h \
        faculdade.h \
        intfaculdade.h \
        pessoa.h \
        professor.h \
        turma.h

FORMS += \
        intfaculdade.ui

RESOURCES += \
    resource.qrc
