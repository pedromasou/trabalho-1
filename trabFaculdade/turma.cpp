#include "turma.h"


Turma::Turma(QString p_codigoTurma,Professor *Prof, QString p_codigoDisciplina)
{
    QByteArray codigo_Turma = p_codigoTurma.toUtf8();
    codigoTurma[qMin(5,codigo_Turma.size())]='\0';
    std::copy(codigo_Turma.constBegin(),
              codigo_Turma.constBegin()+qMin(5,codigo_Turma.size()),
              codigoTurma);

    QByteArray codigo_Disc = p_codigoDisciplina.toUtf8();
    codigoDisc[qMin(10,codigo_Turma.size())]='\0';
    std::copy(codigo_Disc.constBegin(),
              codigo_Disc.constBegin()+qMin(10,codigo_Disc.size()),
              codigoDisc);

    addProf(Prof);
}

void Turma::addAluno(Aluno *Alu)
{
    ListaAluno.append(Alu);
}
void Turma::addProf(Professor *Prof){
    ProfessorResponsavel=Prof;
}

Aluno* Turma::alunoJaINserido(QString p_codigoAluno){
    for(int i=0;i<ListaAluno.size();i++){
        if(ListaAluno.at(i)->matriculaALUNO==p_codigoAluno)
            return ListaAluno.at(i);
        }
    return nullptr;
}
Professor* Turma::getProfResponsavel(){
    return ProfessorResponsavel;
}
