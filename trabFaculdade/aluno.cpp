#include "aluno.h"

Aluno::Aluno()
{
    *Nome='\0';
    *CPF='\0';
    *matriculaALUNO='\0';

}

Aluno::Aluno (QString p_cpf,QString p_nome, QString p_MatriculaALUNO, QString p_codCurso)
{
    const QByteArray Nome_PROF = p_nome.toUtf8();
    Nome[qMin(30,Nome_PROF.size())]='\0';
    std::copy(Nome_PROF.constBegin(),
              Nome_PROF.constBegin()+qMin(30,Nome_PROF.size()),
              Nome);

    const QByteArray cpf_PROF = p_cpf.toUtf8();
    CPF[qMin(12,cpf_PROF.size())]='\0';
    std::copy(cpf_PROF.constBegin(),
              cpf_PROF.constBegin()+qMin(12,cpf_PROF.size()),
              CPF);

    const QByteArray Matricula_ALU = p_MatriculaALUNO.toUtf8();
    matriculaALUNO[qMin(5,Matricula_ALU.size())]='\0';
    std::copy(Matricula_ALU.constBegin(),
              Matricula_ALU.constBegin()+qMin(5,Matricula_ALU.size()),
              matriculaALUNO);

    const QByteArray cd_Curso = p_codCurso.toUtf8();
    codCurso[qMin(8,cd_Curso.size())]='\0';
    std::copy(cd_Curso.constBegin(),
              cd_Curso.constBegin()+qMin(8,cd_Curso.size()),
              codCurso);
}
