#include "disciplina.h"

Disciplina::Disciplina()
{

}

Disciplina::Disciplina(QString p_codigoDisciplina, QString p_nomeDisciplina, QString p_codCurso)
{
    const QByteArray DisciplinaCOD = p_codigoDisciplina.toUtf8();
    CodigoDisc[qMin(10,DisciplinaCOD.size())]='\0';
    std::copy(DisciplinaCOD.constBegin(),
              DisciplinaCOD.constBegin()+qMin(10,DisciplinaCOD.size()),
              CodigoDisc);

    const QByteArray DisciplinaNAM = p_nomeDisciplina.toUtf8();
    nomeDisc[qMin(60,DisciplinaNAM.size())]='\0';
    std::copy(DisciplinaNAM.constBegin(),
              DisciplinaNAM.constBegin()+qMin(60,DisciplinaNAM.size()),
              nomeDisc);

    const QByteArray DisciplinaCUR = p_codCurso.toUtf8();
    codCurso[qMin(8,DisciplinaCUR.size())]='\0';
    std::copy(DisciplinaCUR.constBegin(),
              DisciplinaCUR.constBegin()+qMin(8,DisciplinaCUR.size()),
              codCurso);
}

void Disciplina::addTurma(Turma *Turmas_Disci){
    Lista_Turmas.append(Turmas_Disci);
}

Turma* Disciplina::TurmaJaInserida(QString p_CodigoTurmas){
    for(int i=0;i<Lista_Turmas.size();i++){
        if(Lista_Turmas.at(i)->codigoTurma==p_CodigoTurmas)
            return Lista_Turmas.at(i);
        }
    return nullptr;

}
