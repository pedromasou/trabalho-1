#ifndef CURSO_H
#define CURSO_H
#include <aluno.h>
#include <disciplina.h>
#include <pessoa.h>
#include <professor.h>
#include <turma.h>
#include <QObject>
#include <QVector>
#include <QList>

class Curso : public QObject
{
    Q_OBJECT
public:

    char codigoCurso[8];
    char nomeCurso[50];

    Curso();
    Curso(QString p_codigoCurso, QString p_nomeCurso);
    void addDisciplina(Disciplina *Disci);
    void addAluno(Aluno *Alu);
    void addProf(Professor *Prof);

    Disciplina* disciplinaJaInserida(QString p_codigoDisc);
    Aluno* alunoJaINserido(QString p_codigoAluno);
    Professor* professorJaInserido(QString p_matriculaProf);

    private:
        QList<Aluno*> ListaAluno;
        QList <Professor*> Lista_Professor;
        QList<Disciplina*>Lista_Disciplina;

};

#endif // CURSO_H

