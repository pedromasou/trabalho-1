#ifndef TURMA_H
#define TURMA_H
#include "professor.h"
#include "aluno.h"
#include <QList>



class Turma:public QObject
{
    Q_OBJECT
public:
    Turma();
    Turma(QString p_codigoTurma,Professor *Prof,QString p_codigoDisciplina);
    char codigoTurma[5];
    char codigoDisc[10];
    void addAluno(Aluno *Alu);
    void addProf(Professor *Prof);
    Professor* getProfResponsavel();
    Aluno* alunoJaINserido(QString p_codigoAluno);

//private:
    QList<Aluno*> ListaAluno;
    Professor *ProfessorResponsavel;
};

#endif // TURMA_H
