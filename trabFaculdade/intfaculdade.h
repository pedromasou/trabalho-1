#ifndef INTFACULDADE_H
#define INTFACULDADE_H

#include <QMainWindow>
#include "faculdade.h"

namespace Ui {
class IntFaculdade;
}

class IntFaculdade : public QMainWindow
{
    Q_OBJECT

public:
    explicit IntFaculdade(QWidget *parent = 0);
    ~IntFaculdade();

    void atualizarCB();

private slots:
    void on_addProfessor_clicked();

    void on_cadastrarCurso_clicked();

    void on_okCurso_clicked();

    void on_okDisciplina_clicked();

    void on_okTurma_clicked();

    void on_addAluno_clicked();

    void on_actionCadastro_triggered();

    void on_actionMatricula_triggered();

    void on_actionBancoDados_triggered();

    void on_cadastrarAluno_clicked();

    void on_cadastrarProfessor_clicked();

    void on_cadastrarDisciplina_clicked();

    void on_cbCurso_currentIndexChanged(int index);

    void on_cbDisciplina_currentIndexChanged(int index);

    void on_clearCurso_clicked();

    void on_clearAluno_clicked();

    void on_clearProfessor_clicked();

    void on_clearDisciplina_clicked();

    void on_cadastrarTurma_clicked();

    void on_clearTurma_clicked();

private:
    Ui::IntFaculdade *ui;

    Faculdade oFaculdade;
    void resetAllCursosCb();
    void resetAllAlunosCb();
    void resetAllProfessoresCb();
    void resetAllDisciplinasCb();
    void resetAllTurmasCb();

    void resetMatriculaAlunosCb(QString *cdCurso);
    void resetMatriculaProfessoresCb(QString *cdCurso);
    void resetMatriculaDisciplinasCb(QString *cdCurso);
    void resetMatriculaTurmasCb(QString *cdDisciplina);


    void cleanCurso();
    void cleanAluno();
    void cleanProfessor();
    void cleanDisciplina();
    void cleanTurma();
};

#endif // INTFACULDADE_H
